
variable "ENVIRONMENT" {
    type    = string
    default = "development"
}

variable "AMIS" {
    type = map
    default = {
        ap-south-1 = "ami-06984ea821ac0a879"
        ap-south-1 = "ami-01a4f99c4ac11b03c"
    }
}

variable "AWS_REGION" {
default = "ap-south-1"
}

variable "INSTANCE_TYPE" {
  default = "t2.micro"
}